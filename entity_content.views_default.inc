<?php
/**
 * @file
 * entity_content.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function entity_content_views_default_views() {
  $export = array();
  foreach (entity_content_supported_entity_types() as $entity_type => $entity_info) {

    $view = new view();
    $view->name = 'entity_content_' . $entity_info['type plural'];
    $view->description = '';
    $view->tag = 'entity_content';
    $view->base_table = $entity_info['base table'];
    $view->human_name = 'Entity Content: ' . $entity_info['label plural'];
    $view->core = 7;
    $view->api_version = '3.0';
    $view->disabled = FALSE;

    /* Display: Master */
    $handler = $view->new_display('default', 'Master', 'default');
    $handler->display->display_options['title'] = $entity_info['label plural'];
    $handler->display->display_options['use_more_always'] = FALSE;
    $handler->display->display_options['access']['type'] = 'none';
    $handler->display->display_options['cache']['type'] = 'none';
    $handler->display->display_options['query']['type'] = 'views_query';
    $handler->display->display_options['exposed_form']['type'] = 'basic';
    $handler->display->display_options['pager']['type'] = 'full';
    $handler->display->display_options['pager']['options']['items_per_page'] = '50';
    $handler->display->display_options['style_plugin'] = 'table';
    $handler->display->display_options['style_options']['columns'] = array();
    $handler->display->display_options['style_options']['info'] = array();

    /* Field: Entity: Id */
    $entity_id_field = $entity_info['entity keys']['id'];
    $handler->display->display_options['style_options']['columns']['id'] = $entity_id_field;
    $handler->display->display_options['style_options']['info']['id'] = array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    );
    $handler->display->display_options['fields'][$entity_id_field]['id'] = 'id';
    $handler->display->display_options['fields'][$entity_id_field]['table'] = $entity_info['base table'];
    $handler->display->display_options['fields'][$entity_id_field]['field'] = $entity_id_field;

    // Use a dummy entity to try to create generic entity path pattern.
    $entity_uri = '';
    if (!empty($entity_info['uri callback']) && function_exists($entity_info['uri callback'])) {
      $entity_values = array($entity_id_field => 54321);
      if (!empty($entity_info['entity keys']['bundle'])) {
        $entity_values[$entity_info['entity keys']['bundle']] = $entity_type;
      }
      $entity = entity_create($entity_type, $entity_values);
      $entity_uri = call_user_func($entity_info['uri callback'], $entity);
      $entity_uri = str_replace('54321', '[' . $entity_id_field . ']', $entity_uri['path']);
    }

    /* Field: Entity: Title */
    if (!empty($entity_info['entity keys']['label'])) {
      $title_field = $entity_info['entity keys']['label'];
      $handler->display->display_options['style_options']['columns']['title'] = $title_field;
      $handler->display->display_options['style_options']['info']['title'] = array(
        'sortable' => 1,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      );
      $handler->display->display_options['fields'][$title_field]['id'] = 'title';
      $handler->display->display_options['fields'][$title_field]['table'] = $entity_info['base table'];
      $handler->display->display_options['fields'][$title_field]['field'] = $title_field;
      // Turn title into a link only if entity URI was generated successfully.
      if (!empty($entity_uri)) {
        $handler->display->display_options['fields'][$title_field]['alter']['make_link'] = TRUE;
        $handler->display->display_options['fields'][$title_field]['alter']['path'] = $entity_uri;
      }
    }

    /* Field: Entity: Type */
    if (!empty($entity_info['entity keys']['bundle'])) {
      $type_field = $entity_info['entity keys']['bundle'];
      $handler->display->display_options['style_options']['columns']['type'] = $type_field;
      $handler->display->display_options['style_options']['info']['type'] = array(
        'sortable' => 1,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      );
      $handler->display->display_options['fields'][$type_field]['id'] = 'type';
      $handler->display->display_options['fields'][$type_field]['table'] = $entity_info['base table'];
      $handler->display->display_options['fields'][$type_field]['field'] = $type_field;
      $handler->display->display_options['fields'][$type_field]['label'] = 'Type';
    }

    /* Field: User: Name */
    if (in_array('uid', $entity_info['schema_fields_sql']['base table'])) {
      $handler->display->display_options['style_options']['columns']['name'] = 'name';
      $handler->display->display_options['style_options']['info']['name'] = array(
        'sortable' => 1,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      );
      $handler->display->display_options['fields']['name']['id'] = 'name';
      $handler->display->display_options['fields']['name']['table'] = 'users';
      $handler->display->display_options['fields']['name']['field'] = 'name';
      $handler->display->display_options['fields']['name']['relationship'] = 'uid';
      $handler->display->display_options['fields']['name']['label'] = 'Author';
    }

    /* Field: Entity: Changed */
    if (in_array('changed', $entity_info['schema_fields_sql']['base table'])) {
      $handler->display->display_options['style_options']['columns']['changed'] = 'changed';
      $handler->display->display_options['style_options']['info']['changed'] = array(
        'sortable' => 1,
        'default_sort_order' => 'desc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      );
      $handler->display->display_options['style_options']['default'] = 'changed';
      $handler->display->display_options['fields']['changed']['id'] = 'changed';
      $handler->display->display_options['fields']['changed']['table'] = $entity_info['base table'];
      $handler->display->display_options['fields']['changed']['field'] = 'changed';
      $handler->display->display_options['fields']['changed']['label'] = 'Updated';
      $handler->display->display_options['fields']['changed']['date_format'] = 'short';
    }

    /* Field: Entity: Language */
    if (field_has_translation_handler($entity_type) && in_array('language', $entity_info['schema_fields_sql']['base table'])) {
      $handler->display->display_options['style_options']['columns']['language'] = 'language';
      $handler->display->display_options['style_options']['info']['language'] = array(
        'sortable' => 1,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      );
      $handler->display->display_options['fields']['language']['id'] = 'language';
      $handler->display->display_options['fields']['language']['table'] = $entity_info['base table'];
      $handler->display->display_options['fields']['language']['field'] = 'language';
      $handler->display->display_options['fields']['language']['label'] = 'Language';
    }

    /* Field: Global: Custom text */
    // Display operation links only if entity URI was generated successfully.
    if (!empty($entity_uri)) {
      $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
      $handler->display->display_options['fields']['nothing']['table'] = 'views';
      $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
      $handler->display->display_options['fields']['nothing']['label'] = 'Operations';
      // This is quite dirty way of generating Operations links, which assumes
      // that edit and delete links are just entity view link with added '/edit'
      // or '/delete' at the end of the URL - which is kinda default, but is not
      // always the case. Still, this seems to be a better approach than trying
      // to use Views' 'edit_link' and 'delete_link' pseudo-fields, as they will
      // work for ECK (see https://drupal.org/node/1424738), but is not guaranteed
      // to work for other custom entity types (see for example
      // https://drupal.org/comment/6123488 and https://drupal.org/comment/6213860
      // for more info). Basically, this approach should work in more cases.
      // Also, we need this funny way of putting together the links, using this
      // dreadful rawurldecode() below, as we still have [id] token there, which
      // otherwise would be messed up (encoded).
      $links = array(
        'edit' => '<a href="' . rawurldecode(url($entity_uri . '/edit')) . '">' . t('edit') . '</a>',
        'delete' => '<a href="' . rawurldecode(url($entity_uri . '/delete')) . '">' . t('delete') . '</a>',
      );
      $handler->display->display_options['fields']['nothing']['alter']['text'] = implode(' &nbsp; ', $links);
    }

    /* No results behavior: Global: Text area */
    $handler->display->display_options['empty']['area']['id'] = 'area';
    $handler->display->display_options['empty']['area']['table'] = 'views';
    $handler->display->display_options['empty']['area']['field'] = 'area';
    $handler->display->display_options['empty']['area']['empty'] = TRUE;
    $handler->display->display_options['empty']['area']['content'] = 'No ' . $entity_info['label plural'] . ' available.';
    $handler->display->display_options['empty']['area']['format'] = 'filtered_html';

    /* Relationship: Entity: Author */
    if (in_array('uid', $entity_info['schema_fields_sql']['base table'])) {
      $handler->display->display_options['relationships']['uid']['id'] = 'uid';
      $handler->display->display_options['relationships']['uid']['table'] = $entity_info['base table'];
      $handler->display->display_options['relationships']['uid']['field'] = 'uid';
      $handler->display->display_options['relationships']['uid']['label'] = 'Author';
      $handler->display->display_options['relationships']['uid']['required'] = TRUE;
    }

    /* Filter criterion: Entity: Type */
    if (!empty($entity_info['entity keys']['bundle'])) {
      $handler->display->display_options['filters']['type']['id'] = 'type';
      $handler->display->display_options['filters']['type']['table'] = $entity_info['base table'];
      $handler->display->display_options['filters']['type']['field'] = $entity_info['bundle keys']['bundle'];
      $handler->display->display_options['filters']['type']['exposed'] = TRUE;
      $handler->display->display_options['filters']['type']['expose']['operator_id'] = 'type_op';
      $handler->display->display_options['filters']['type']['expose']['label'] = 'Type';
      $handler->display->display_options['filters']['type']['expose']['operator'] = 'type_op';
      $handler->display->display_options['filters']['type']['expose']['identifier'] = 'type';
    }

    /* Filter criterion: Entity: Language */
    if (field_has_translation_handler($entity_type) && in_array('language', $entity_info['schema_fields_sql']['base table'])) {
      $handler->display->display_options['filters']['language']['id'] = 'language';
      $handler->display->display_options['filters']['language']['table'] = $entity_info['base table'];
      $handler->display->display_options['filters']['language']['field'] = 'language';
      $handler->display->display_options['filters']['language']['exposed'] = TRUE;
      $handler->display->display_options['filters']['language']['expose']['operator_id'] = 'language_op';
      $handler->display->display_options['filters']['language']['expose']['label'] = 'Language';
      $handler->display->display_options['filters']['language']['expose']['operator'] = 'language_op';
      $handler->display->display_options['filters']['language']['expose']['identifier'] = 'language';
    }

    /* Display: Products */
    $handler = $view->new_display('page', $entity_info['label plural'], 'page');
    $handler->display->display_options['display_description'] = 'An admin page listing all ' . $entity_info['label'] . ' entities';
    $handler->display->display_options['path'] = 'admin/content/' . $entity_info['type plural'];
    $handler->display->display_options['menu']['type'] = 'normal';
    $handler->display->display_options['menu']['title'] = $entity_info['label plural'];
    $handler->display->display_options['menu']['weight'] = '0';
    $handler->display->display_options['menu']['name'] = 'management';
    $handler->display->display_options['menu']['context'] = 0;
    $handler->display->display_options['menu']['context_only_inline'] = 0;
    $translatables['admin_content_' . $entity_info['type plural']] = array(
      t('Master'),
      t($entity_info['label plural']),
      t('more'),
      t('Apply'),
      t('Reset'),
      t('Sort by'),
      t('Asc'),
      t('Desc'),
      t('Items per page'),
      t('- All -'),
      t('Offset'),
      t('« first'),
      t('‹ previous'),
      t('next ›'),
      t('last »'),
      t('Author'),
      t('Id'),
      t('.'),
      t(','),
      t('Title'),
      t('Type'),
      t('Updated'),
      t('Language'),
      t('Operations'),
      t('An admin page listing all ' . $entity_info['label'] . ' entities'),
    );

    $export['entity_content_' . $entity_info['type plural']] = $view;

  }
  return $export;
}
