<?php

/**
 * @file
 * entity_content.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function entity_content_menu_default_menu_links() {
  $menu_links = array();
  foreach (entity_content_supported_entity_types() as $entity_type => $entity_info) {

    // Exported menu link: management_<entity_types>:admin/content/<entity_types>
    $menu_links['management:admin/content/' . $entity_info['type plural']] = array(
      'menu_name' => 'management',
      'link_path' => 'admin/content/' . $entity_info['type plural'],
      'router_path' => 'admin/content/' . $entity_info['type plural'],
      'link_title' => $entity_info['label plural'],
      'module' => 'system',
      'hidden' => 0,
      'external' => 0,
      'has_children' => 0,
      'expanded' => 0,
      'weight' => 0,
      'customized' => 0,
      'parent_path' => 'admin/content',
    );
    // Translatables
    // Included for use with string extractors like potx.
    t($entity_info['label plural']);

  }
  return $menu_links;
}
