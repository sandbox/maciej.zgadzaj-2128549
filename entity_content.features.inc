<?php

/**
 * @file
 * entity_content.features.inc
 */

/**
 * Implements hook_views_api().
 */
function entity_content_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
